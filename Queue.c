#include "Queue.h"
#include <stdio.h>


Stack* addToStack(Stack *st, void* el){
	Stack *s = malloc(sizeof(Stack));
	s->el = el;
	s->next = st;
	return s;
}
Stack *popFromStack(Stack*st, void **el){
	Stack *s = NULL;
	if(st){
		*el = st->el;
		s = st->next;
		free(st);
	}
	return s;
}
int qisEmpty(Queue*q){
	if(!q->s1&&!q->s2)
		return 1;
	return 0;
}
Queue CreateQueue(){
	Queue q;
	memset(&q, 0, sizeof(Queue));
	return q;
}
int enquere(Queue *q, void* el){
	q->s1 = addToStack(q->s1,el);
}
void* dequere(Queue *q){
	void* el =0;
	if(!q->s2){
		if(!q->s1)
			return NULL;
		while(q->s1){
			q->s1 = popFromStack(q->s1, &el);
			q->s2 = addToStack(q->s2, el);
		}
	}
	q->s2 = popFromStack(q->s2, &el);
	return el;
}
