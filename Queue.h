#ifndef QUEUE_H
#define QUEUE_H

typedef struct Stack{
	void* el;
	struct Stack *next;
}Stack;

typedef struct Queue{
	Stack *s1;
	Stack *s2;
}Queue;

Stack* addToStack(Stack *st, void* el);
Stack *popFromStack(Stack*st, void **el);

int qisEmpty(Queue*q);
Queue CreateQueue();
int enquere(Queue *q, void* el);
void* dequere(Queue *q);

#endif
