#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"

#define FILE_NAME_SIZE 128


typedef struct{
	int min;
	int hour;
} Time;


typedef struct{
	int day;
	int mounth;
	int year;
} Date;
typedef struct File{
	char *name;
	int size;
	Date crDate;
	Time crTime;
} File;
char* getFileName(){
	char* str;
	printf("Enter filename: ");
	str = (char*)calloc(50, sizeof(char));
	fgets(str, 50*sizeof(char), stdin);
	int len = strlen(str);
	if(str[len-1]=='\n')
		str[len-1] = 0;
	return str;
}

int isCorrectDate(Date* date){
	if(date->mounth < 1|| date->mounth > 12)
		return 0;

	int mounth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	if(!(date->year%4)&&(date->year%100)|| !(date->year%400))
		mounth[1]++;

	if(date->day < 1 || date->day > mounth[date->mounth-1])
		return 0;
	return 1;
}
int isCorrectTime(Time* time){
	if(time->min < 0 || time->min > 59)
		return 0;
	if(time->hour < 0 || time->hour > 23)
		return 0;
	return 1;
}
int parseStr(char* src, void* dst){
	char *e;
	int len;
	while(isspace(*src))
		src++;

	len = strlen(src);
	e=src+len-1;
	while(isspace(*e)){
		e--;
		if(e<src)
			return -1;
	}
	*(e+1)='\0';
	strcpy(dst,src);
	return 0;
}
int getDateById(Date * date, int id){
	switch(id){
			case 0:
				return date->year;
			case 1:
				return date->mounth;
			case 2:
				return date->day;
		}
	return 0;
}

int getTimeById(Time * time, int id){
	switch(id){
			case 0:
				return time->hour;
			case 1:
				return time->min;
		}
	return 0;
}

int compareSizes(File *f1, File *f2, int decrease){
	return ((decrease)? -1: 1) * (f1->size - f2->size);
}
int compareNames(File *f1, File *f2, int decrease){
	return ((decrease)? -1: 1) * strcmp(f1->name, f2->name);
}
int compareDates(File *f1, File *f2, int decrease){
	int d1, d2;
	int i;
	for(i = 0; i < 3; i++){
		d1 = getDateById(&f1->crDate, i);
		d2 = getDateById(&f2->crDate, i);
		if(d1!=d2){
			return ((decrease)? -1: 1) * (d1-d2);
		}
	}
	return 0;
}
int parseName(char* src, void* dst){
	((File*) dst)->name =(char*) calloc(FILE_NAME_SIZE, sizeof(char));
	return parseStr(src, ((File*) dst)->name);
}
int parseNum(char* src, void* dst){
	char c;
	if(sscanf(src, "%d %c", (int*) dst, &c)!=1){
		return -1;
	}
	return 0;
}
int parseSize(char* src, void* dst){
	return parseNum(src, &((File*) dst)->size);
}
int parseDate(char* src, void* dst){
	Date* date = &((File*) dst)->crDate;
	char c;
	if(sscanf(src, "%d.%d.%d %c", &date->day, &date->mounth, &date->year, &c)!=3){
		return -1;
	}
	if(!isCorrectDate(date)){
		return -1;
	}
	return 0;
}

int parseTime(char* src, void* dst){
	Time* time = &((File*) dst)->crTime;
	char c;
	if(sscanf(src, "%d:%d %c", &time->hour, &time->min, &c)!=2)
		return -1;
	if(!isCorrectTime(time)){
		return -1;
	}

	return 0;

}
int parseFull(char* src, void* dst){
    File* file = (File*)dst;
    char* e;
    char* m;
    e=strstr(src, ", ");
    if(!e)
        return -1;
    *e='\0';
    if(parseName(src, dst))
        return -1;
//    printf("1\n");
    m=e+2;

    e=strstr(m, ", ");

    if(!e)
        return -1;
    *e='\0';
    if(parseSize(m, dst))
        return -1;
 //   printf("2\n");
    m=e+2;
    e=strstr(m, " ");
    if(!e)
        return -1;
    *e='\0';
    if(parseDate(m, dst))
        return -1;
 //   printf("3\n");
 //   printf("%s\n", e+1);
    if(parseTime(e+1, dst))
        return -1;
    return 0;
}
void writeFileInfo(File * file){
	printf("name % 30s size % 8d date %02d-%02d-%04d time %02d:%02d\n", file->name, file->size,
		file->crDate.day, file->crDate.mounth, file->crDate.year,
		file->crTime.hour, file->crTime.min);
}
int main()
{
	char* name = getFileName();
	char buf[500];
	FILE* file = fopen(name, "r");
	if(!file){
		printf("Cannot open file\n");
		return 0;
	}
	File *fl = malloc(sizeof(File));
	File *f = fl;
	Queue q = CreateQueue();
	while(!feof(file)){
		fgets(buf, 500, file);
		if(buf[strlen(buf)-1]=='\n')
			buf[strlen(buf)-1] = 0;
		if(!parseFull(buf, fl)){
			enquere(&q, fl);
			f = fl;
			fl = malloc(sizeof(File));
		}
			//return -1;
			//printf("1\n");
	}
	//File tmp;
	//memcpy(&tmp, fl, sizeof(File));
	writeFileInfo(f);
	free(fl);

	while(!qisEmpty(&q)){
		fl = dequere(&q);
		if(compareDates(fl, f, 0)==0)
			writeFileInfo(fl);
	}

    printf("Hello world!\n");
    return 0;
}
